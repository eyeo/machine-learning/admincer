# Copyright (C) 2019-present eyeo GmbH
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the “Software”), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
from setuptools import setup

README_PATH = os.path.join(os.path.dirname(__file__), 'README.md')

with open(README_PATH) as f:
    long_description = f.read()

setup(
    name='admincer',
    use_scm_version=True,
    description='Tool for managing datasets for visual ad detection',
    long_description=long_description,
    long_description_content_type='text/markdown',
    author='eyeo GmbH',
    author_email='info@adblockplus.org',
    url='https://gitlab.com/eyeo/machine-learning/admincer/',
    packages=['admincer'],
    entry_points={
        'console_scripts': ['admincer=admincer.__main__:main'],
    },
    include_package_data=True,
    install_requires=[
        'Pillow',
        'opencv-python',
        'setuptools_scm',
    ],
    setup_requires=[
        'setuptools_scm',
    ],
    license='MIT',
    zip_safe=False,
    keywords='ad-detection machine-learning dataset',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Operating System :: OS Independent',
        'Topic :: Utilities',
    ],
)

# Copyright (C) 2019-present eyeo GmbH
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the “Software”), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Tests for region extraction."""

import pytest

import admincer.index as idx
import admincer.extract as ex

from conftest import Colors as cc


@pytest.mark.parametrize('region_type, expected_params', [
    ('r0', []),
    ('r1', [(10, 20, cc.WHITE), (10, 10, cc.BLUE), (10, 10, cc.BLUE)]),
    ('r2', [(30, 40, cc.WHITE)]),
])
def test_extract(regdir, region_type, expected_params):
    ri = idx.reg_index(str(regdir))
    extracted_images = [
        image for box, image in ex.extract_regions(ri, region_type)
    ]
    assert len(extracted_images) == len(expected_params)
    extracted_params = {
        image.size + (image.getpixel((0, 0)),)
        for image in extracted_images
    }
    assert extracted_params == set(expected_params)

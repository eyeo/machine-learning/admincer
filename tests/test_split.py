# Copyright (C) 2019-present eyeo GmbH
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the “Software”), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Tests for splitting."""

import os
import pytest

import admincer.index as idx
import admincer.split as sp


def _ensure_symlink(path, index):
    for img in index.keys():
        assert os.path.islink(os.path.join(path, img))
    return True


def test_split1(regdir_yolo_txt, tmpdir):
    """Split a percentage of source images into another folder."""
    src_index = idx.reg_index(str(regdir_yolo_txt))
    dest = tmpdir.join('split1')
    sp.split1(regdir_yolo_txt, src_index, dest)
    if any(f.endswith('.txt') for f in os.listdir(dest)):
        assert 'class.names' in os.listdir(dest)
    assert len(idx.reg_index(dest)) == 1
    assert len(idx.reg_index(str(regdir_yolo_txt))) == 9


def test_split2(regdir_yolo_txt, tmpdir):
    """Split source images; copy into two destination folders."""
    src_index = idx.reg_index(str(regdir_yolo_txt))
    dest1 = tmpdir.join('split1')
    dest2 = tmpdir.join('split2')
    sp.split2(regdir_yolo_txt, src_index, dest1, dest2, 20)
    assert len(idx.reg_index(dest1)) == 8
    assert len(idx.reg_index(dest2)) == 2
    assert len(idx.reg_index(str(regdir_yolo_txt))) == 10
    if any(f.endswith('.txt') for f in os.listdir(dest1)):
        assert 'class.names' in os.listdir(dest1)
    if any(f.endswith('.txt') for f in os.listdir(dest2)):
        assert 'class.names' in os.listdir(dest2)


def test_split2_symlink(regdir_yolo_txt, tmpdir):
    """Split images into 2 different directories with symlinks."""
    src_index = idx.reg_index(str(regdir_yolo_txt))
    dest1 = tmpdir.join('split1')
    dest2 = tmpdir.join('split2')
    sp.split2(regdir_yolo_txt, src_index, dest1, dest2, 20, True)
    index1 = idx.reg_index(dest1)
    index2 = idx.reg_index(dest2)
    assert len(index1) == 8
    assert len(index2) == 2
    if any(f.endswith('.txt') for f in os.listdir(dest1)):
        assert 'class.names' in os.listdir(dest1)
    if any(f.endswith('.txt') for f in os.listdir(dest2)):
        assert 'class.names' in os.listdir(dest2)
    assert _ensure_symlink(dest1, index1)
    assert _ensure_symlink(dest2, index2)


def test_split1_csv_regions_fail(regdir, tmpdir):
    """Must use YOLO (.txt) format for single destination directory."""
    src_index = idx.reg_index(str(regdir))
    dest = tmpdir.join('split1')
    with pytest.raises(Exception) as info:
        sp.split1(regdir, src_index, dest, 66)
    assert str(info.value).startswith('Single destination directory requires')

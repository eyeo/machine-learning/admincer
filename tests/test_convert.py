# Copyright (C) 2019-present eyeo GmbH
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the “Software”), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Tests for converting annotations from CVAT to YOLO format."""


import pytest

import admincer.convert as co
import admincer.index as idx


def test_process_cvat_no_output_dir(convert_src):
    co.process_cvat_xml(str(convert_src.join('14_task11.xml')))

    results = idx.reg_index(str(convert_src.join('task11')))
    assert results['1_cut_1.png'] == [(976, 461, 1278, 711, 'textad')]
    assert results['1_cut_0.png'] == [(233, 714, 962, 805, 'bannerad'),
                                      (59, 206, 215, 805, 'bannerad'),
                                      (974, 556, 1278, 807, 'bannerad')]
    names = 'bannerad\ntextad'
    assert convert_src.join('task11', 'class.names').read() == names


def test_process_cvat_with_output_dir_and_copy(convert_src):
    co.process_cvat_xml(str(convert_src.join('14_task11.xml')),
                        str(convert_src.join('converted')), False, True)

    results = idx.reg_index(str(convert_src.join('converted')))
    assert results['1_cut_0.png'] == [(233, 714, 962, 805, 'bannerad'),
                                      (59, 206, 215, 805, 'bannerad'),
                                      (974, 556, 1278, 807, 'bannerad')]
    assert convert_src.join('converted', '1_cut_0.png').check(exists=1)
    assert convert_src.join('task11', '1_cut_0.png').check(exists=1)
    assert convert_src.join('converted', '1_cut_1.png').check(exists=1)
    assert convert_src.join('task11', '1_cut_1.png').check(exists=1)
    assert convert_src.join('converted', 'no_ads.png').check(exists=1)


def test_process_cvat_with_outputdir_names_copy(convert_src_names):
    convert_src_names.join('converted', 'foo.names').write('bar', ensure=True)

    co.process_cvat_xml(str(convert_src_names.join('14_task11.xml')),
                        str(convert_src_names.join('converted')), False, True)
    co.process_cvat_xml(str(convert_src_names.join('15_task12.xml')),
                        str(convert_src_names.join('converted')), False, True)

    results = idx.reg_index(str(convert_src_names.join('converted')))
    assert results['2_cut_0.png'] == [(957, 807, 1262, 1348, 'bannerad')]
    assert results['5_cut_1.png'] == [(975, 257, 1219, 468, 'popupad')]
    assert convert_src_names.join('converted', '2_cut_0.png').check(exists=1)
    assert convert_src_names.join('task12', '2_cut_0.png').check(exists=1)
    assert convert_src_names.join('converted', '5_cut_1.png').check(exists=1)
    assert convert_src_names.join('task12', '5_cut_1.png').check(exists=1)

    names = 'bar\nbannerad\ntextad\npopupad'
    assert convert_src_names.join('converted', 'foo.names').read() == names
    assert convert_src_names.join('task12', 'class.names').read() == names


def test_image_missing(bad_convert_src, caplog):
    co.process_cvat_xml(str(bad_convert_src.join('14_task11.xml')))
    assert 'image cannot be found.' in caplog.records[0].msg

    results = idx.reg_index(str(bad_convert_src.join('task11')))
    assert results['1_cut_0.png'] == [(233, 714, 962, 805, 'bannerad'),
                                      (59, 206, 215, 805, 'bannerad'),
                                      (974, 556, 1278, 807, 'bannerad')]


def test_convert_2names(convert_src_names, caplog):
    convert_src_names.join('task11', 'other.names').write('foo')

    with pytest.raises(Exception) as info:
        co.process_cvat_xml(
            str(convert_src_names.join('14_task11.xml')),
            str(convert_src_names.join('converted')), False, True)

    assert str(info.value).startswith('Multiple .names files')

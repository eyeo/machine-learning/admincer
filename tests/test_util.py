# Copyright (C) 2019-present eyeo GmbH
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the “Software”), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Tests for common utilities."""

import pytest

import admincer.find as fnd
import admincer.util as util


@pytest.mark.parametrize('source,parsed', [
    ('foo:10x10', fnd.RegionQuery('foo', 10, 10)),
    ('bar:100x100:50', fnd.RegionQuery('bar', 100, 100, 50)),
    ('broken', None),
    ('broken:100', None),
    ('broken:cxz', None),
    ('broken:10x10:10:5', None),
])
def test_region_parser(source, parsed):
    if parsed is None:
        with pytest.raises(Exception):
            util.region(source)
    else:
        got = util.region(source)
        assert got.__dict__ == parsed.__dict__


@pytest.mark.parametrize('source,parsed', [
    ('foo/bar.png', fnd.FragmentQuery('foo/bar.png')),
    ('/a/b/c.jpg:42', fnd.FragmentQuery('/a/b/c.jpg', 42)),
    ('broken:1:2', None),
    ('broken:foo', None),
])
def test_fragment_parser(source, parsed):
    if parsed is None:
        with pytest.raises(Exception):
            util.fragment(source)
    else:
        got = util.fragment(source)
        assert got.__dict__ == parsed.__dict__

# Copyright (C) 2019-present eyeo GmbH
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the “Software”), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Tests for slicing."""

import pytest

import admincer.index as idx
import admincer.slice as slc

from conftest import image_to_chars, SLICE_STRINGS


SLICE_EXPECT = {}

SLICE_EXPECT['default'] = {
    '0-0.png': {
        'chars': SLICE_STRINGS[0],
        'regions': [(0, 0, 10, 10, 'r1'), (20, 10, 30, 30, 'r2')],
    },
    '1-0.png': {
        'chars': SLICE_STRINGS[1],
        'regions': [],
    },
    '2-0.png': {
        'chars': SLICE_STRINGS[2][:9],
        'regions': [(0, 0, 10, 10, 'r1'), (20, 0, 30, 20, 'r3')],
    },
    '2-10.png': {
        'chars': SLICE_STRINGS[2][3:],
        'regions': [(20, 0, 30, 10, 'r3'), (20, 20, 30, 30, 'r1')],
    },
    '3-0.png': {
        'chars': SLICE_STRINGS[3][:9],
        'regions': [(20, 10, 30, 30, 'r1')],
    },
    '3-10.png': {
        'chars': SLICE_STRINGS[3][3:12],
        'regions': [(20, 0, 30, 20, 'r1')],
    },
    '3-20.png': {
        'chars': SLICE_STRINGS[3][6:15],
        'regions': [(20, 0, 30, 10, 'r1')],
    },
    '3-30.png': {
        'chars': SLICE_STRINGS[3][9:18],
        'regions': [],
    },
    '3-40.png': {
        'chars': SLICE_STRINGS[3][12:21],
        'regions': [(20, 20, 30, 30, 'r2')],
    },
    '3-50.png': {
        'chars': SLICE_STRINGS[3][15:],
        'regions': [(20, 10, 30, 30, 'r2')],
    },
}

SLICE_EXPECT['no_empty'] = {
    name: SLICE_EXPECT['default'][name]
    for name in [
        '0-0.png',
        '2-0.png', '2-10.png',
        '3-0.png', '3-10.png', '3-20.png', '3-40.png', '3-50.png',
    ]
}

SLICE_EXPECT['no_empty_80'] = {
    name: SLICE_EXPECT['default'][name]
    for name in ['0-0.png', '2-0.png', '3-0.png', '3-10.png', '3-50.png']
}
SLICE_EXPECT['no_empty_80']['2-10.png'] = {
    'chars': SLICE_STRINGS[2][3:],
    'regions': [(20, 20, 30, 30, 'r1')],
}

SLICE_EXPECT['step_30'] = {
    name: SLICE_EXPECT['default'][name]
    for name in ['0-0.png', '1-0.png', '2-0.png', '3-0.png', '3-30.png']
}

SLICE_EXPECT['empty_percent_35'] = {
    name: SLICE_EXPECT['default'][name]
    for name in [
        '0-0.png', '1-0.png', '2-0.png', '2-10.png', '3-0.png', '3-30.png',
    ]
}

SLICE_EXPECT['empty_percent_10'] = {
    name: SLICE_EXPECT['default'][name]
    for name in [
        '0-0.png', '1-0.png', '2-0.png', '2-10.png', '3-0.png', '3-10.png',
        '3-20.png', '3-40.png', '3-50.png',
    ]
}

SLICE_EXPECT['empty_percent_100'] = {
    name: SLICE_EXPECT['default'][name]
    for name in [
        '1-0.png', '3-30.png',
    ]
}


def check_results(results, expect):
    """Compare the contents of target index with expectations."""
    assert set(results) == set(expect)
    for img_name in results:
        image = results.load_image(img_name)
        assert image.size == (30, 30)
        assert image_to_chars(image, 10, 3, 3) == expect[img_name]['chars']
        assert results[img_name] == expect[img_name]['regions']


@pytest.mark.parametrize('expect_key,kw', [
    ('default', {}),
    ('no_empty', {'empty_percent': 0}),
    ('no_empty_80', {'empty_percent': 0, 'min_part': 0.8}),
    ('step_30', {'step': 30}),
    ('empty_percent_35', {'empty_percent': 35}),
    ('empty_percent_10', {'empty_percent': 10}),
    ('empty_percent_100', {'empty_percent': 100}),
])
def test_slice(slice_source, tmpdir, expect_key, kw):
    """Test slice sub-command."""
    target = tmpdir.mkdir('output')
    source = idx.reg_index(str(slice_source))
    output = idx.reg_index(str(target))
    slc.slice_all(source, output, **kw)
    check_results(output, SLICE_EXPECT[expect_key])


def test_double_slice_fail(slice_source, tmpdir):
    target = tmpdir.mkdir('output')
    source = idx.reg_index(str(slice_source))
    output = idx.reg_index(str(target))
    output['0-0.png'] = []
    with pytest.raises(KeyError):
        slc.slice_all(source, output)

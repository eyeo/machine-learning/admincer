# Copyright (C) 2019-present eyeo GmbH
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the “Software”), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Indexing images in directories."""

import logging
import os

from PIL import Image


def extract_regions(reg_index, region_type):
    """Return the contents of the regions as images.

    Parameters
    ----------
    reg_index : Index
        Index of source images and their regions.
    region_type : str
        Region type to extract.

    Returns
    -------
    regions : iterable(Image)
        Extracted regions as PIL images.

    """
    for rel_path, regions in sorted(reg_index.items()):
        boxes = [r[:4] for r in regions if r[4] == region_type]
        if not boxes:
            continue
        logging.info('### %s', rel_path)
        image_path = os.path.join(reg_index.root_path, rel_path)
        image = Image.open(image_path)
        for box in boxes:
            yield box, image.crop(box)

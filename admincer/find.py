# Copyright (C) 2019-present eyeo GmbH
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the “Software”), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Searching for images based on regions and fragments."""

import functools
import os

import cv2


class Query:
    """Base class for queries."""

    difficulty = 1  # How much compute does this query need?

    def is_match(self, image_path, data):
        """Check if this image matches the query."""
        raise NotImplementedError()  # pragma: no cover

    def __str__(self):
        return self.format_string.format(self)


class RegionQuery(Query):
    """Query for regions of specific type and size."""

    format_string = (
        'RegionQuery({0.region_type}, '
        '{0.min_width} <= width <= {0.max_width}, '
        '{0.min_height} <= height <= {0.max_height})'
    )

    def __init__(self, region_type, width, height, tolerance=25):
        self.region_type = region_type
        multiplier = 1 + tolerance / 100
        self.max_width = width * multiplier
        self.min_width = width / multiplier
        self.max_height = height * multiplier
        self.min_height = height / multiplier

    def is_match(self, image_path, data):
        """Check if there is a matching region (within tolerance)."""
        if not isinstance(data, list):
            raise Exception('Region queries require a region index')
        for x1, y1, x2, y2, region_type in data:
            if (
                region_type == self.region_type
                and self.min_width <= x2 - x1 <= self.max_width
                and self.min_height <= y2 - y1 <= self.max_height
            ):
                return True
        return False


class FragmentQuery(Query):
    """Query for a fragment image."""

    difficulty = 10
    format_string = 'FragmentQuery({0.frag_path}, tolerance={0.tolerance})'

    def __init__(self, frag_path, tolerance=10):
        self.frag_path = frag_path
        self.tolerance = tolerance / 100

    @staticmethod
    @functools.lru_cache(maxsize=1)
    def _load_image(image_path):
        """Cache images between queries."""
        with open(image_path, 'r'):
            pass  # Check that we can read the image (OpenCV doesn't).
        return cv2.imread(image_path)

    def is_match(self, image_path, data):
        """Check if the best template match is below tolerance."""
        if not hasattr(self, 'frag'):
            self.frag = FragmentQuery._load_image(self.frag_path)
        image = FragmentQuery._load_image(image_path)
        res = cv2.matchTemplate(self.frag, image, cv2.TM_SQDIFF_NORMED)
        best = res.min()
        return best < self.tolerance


def find(index, queries):
    """Find images in the index that match all the queries."""
    queries = sorted(queries, key=lambda q: q.difficulty)
    for name in sorted(index):
        path = os.path.join(index.root_path, name)
        data = index[name]
        if all(query.is_match(path, data) for query in queries):
            yield name

# Copyright (C) 2019-present eyeo GmbH
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the “Software”), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Split a set of screenshots into two non-overlapping sets."""

import random

import admincer.index as idx


def split1(src_dir, src_index, dest, percent=10):
    """Move `percent` images and their annotations to `dest`.

    Parameters
    ----------
    src_dir : py._path.local.LocalPath
        The directory containing the source images and annotations.
    src_index : RegIndex
        The index of the source images and annotations.
    dest : py._path.local.LocalPath
        The directory to move the images to.
    percent : int
        The percent of images to move to dest.

    Raises
    ------
    Exception
        If the images are not annotated in YOLO (.txt) format.

    """
    num_to_move = round(len(src_index) * percent / 100)
    dst_idx = idx.reg_index(dest, ensure_dir=True)

    while num_to_move > 0:
        img = random.choice(list(src_index.items()))
        dst_idx.add_image_from(src_index, img, 'move')
        num_to_move -= 1


def split2(src_dir, src_index, dest1, dest2, percent=10, symlink=False):
    """Split the images in src; `percent` to dest2, remaining to dest1.

    Parameters
    ----------
    src_dir : py._path.local.LocalPath
        The directory containing the source images and annotations.
    src_index : RegIndex
        The index of the source images and annotations.
    dest1 : py._path.local.LocalPath
        The folder where 100 - `percent` of images will be moved to.
    dest2 : py._path.local.LocalPath
        The folder where `percent` of images will be moved to.
    percent : int
        The percent of images to move to dest2.
    symlink : bool
        If True, images will be symlinked instead of moved.

    """
    num_to_dest2 = round(len(src_index) * percent / 100)
    regions1 = idx.reg_index(dest1, ensure_dir=True)
    regions2 = idx.reg_index(dest2, ensure_dir=True)

    while num_to_dest2 > 0:
        img = random.choice(list(src_index.items()))
        regions2.add_image_from(
            src_index, img, 'symlink' if symlink else 'copy',
        )
        src_index.pop(img[0])
        num_to_dest2 -= 1

    # Copy or symlink the rest to dest1
    for img in list(src_index.items()):
        regions1.add_image_from(
            src_index, img, 'symlink' if symlink else 'copy',
        )
